	<?php
	// Peticiones de usuario
	$peticiones = array();
	$peticiones['inicio'][] 		= "inicio";
	$peticiones['nosotros'][] 		= 'nosotros';
	$peticiones['metodologia'][] 	= 'metodologia';
	$peticiones['talleres'][] 		= 'talleres';
	$peticiones['galeria'][] 		= 'galeria';
	$peticiones['descargas'][] 	= 'descargas';
	$peticiones['contactos'][] 	= 'contactos';

	// Resultados de las peticiones de usuario
	$condicion = array();
	$condicion['nosotros']		= cadena::peticion($peticiones['nosotros'], 'get', 0);
	$condicion['metodologia'] 	= cadena::peticion($peticiones['metodologia'], 'get', 0);
	$condicion['talleres'] 		= cadena::peticion($peticiones['talleres'], 'get', 0);
	$condicion['galeria'] 		= cadena::peticion($peticiones['galeria'], 'get', 0);
	$condicion['descargas'] 	= cadena::peticion($peticiones['descargas'], 'get', 0);
	$condicion['contactos'] 	= cadena::peticion($peticiones['contactos'], 'get', 0);

	class paginas {
		// Propiedades
			public static $condicion = array();

		// Métodos
		public static function menu() {
			$contenido = "";
			$menu = "php/html/menu.web";
			if (file_exists($menu)) {
				$contenido = file_get_contents($menu);
			}
			echo $contenido;
		}
		public static function inicio() {
			if (count($_GET) === 0) {
				// Comprobar la existencia del archivo
				$fichero = "php/html/inicio.web";
				if (file_exists($fichero)) {
					echo file_get_contents($fichero);
				}
			}
		}
		public static function nosotros() {
			if (self::$condicion["nosotros"] === "verdadero") {
				$fichero = "php/html/nosotros.web";
				if (file_exists($fichero)) {
					echo file_get_contents($fichero);
				}
			}
		}
		public static function metodologia() {
			// Texto que encierra metodología
			if (self::$condicion["metodologia"] === "verdadero") {
				$fichero = "php/html/metodologia.web";
				if (file_exists($fichero)) {
					echo file_get_contents($fichero);
				}
			}
		}
		public static function talleres() {
			// Instrucciones para construir la página talleres
			if (self::$condicion["talleres"] === "verdadero") {
				$fichero = "php/html/talleres.web";
				if (file_exists($fichero)) {
					echo file_get_contents($fichero);
				}
			}
		}
		public static function galeria($path = "") {
			// Instrucciones para construir la página galeria
			if (self::$condicion["galeria"] === "verdadero") {
				$fichero = "php/html/galeria.web";
				if (file_exists($fichero)) {
					// echo file_get_contents($fichero);
				}
				
				// Lectura de archivos
				if (!empty($path)) {
					$ext = array();

					if (file_exists($path)) {
						$dir = opendir($path);
						$archivos = "";

						while ($ficheros = readdir($dir)) {
							// Probar su funcionamiento
							if (($ficheros !== ".") && ($ficheros !== "..")) {
								$ext = explode(".", $ficheros);
								$img = $ext[count($ext) - 1];
								$img = strtolower($img); // La extensión para a ser minúscula

								if ($img === "png" || $img === "jpg" || $img === "jpeg") {
									$archivos .= '
				<div class="foto">
					<div class="caption">' . $ficheros . '</div>
					<div class="imagen"><a href="' . $path . $ficheros . '"><img src="' . $path . $ficheros . '" alt="' . $ficheros . '"></a></div>
				</div>' . "\n" . '
									';
								}
							}
						}
						closedir($dir);

						$contenido = '
		<div class="page">
			<div class="albumes">
				' . $archivos . '
				<div class="limpiar"></div>
			</div>
		</div>' . "\n";

		echo $contenido;

					}else {
						//echo "El archivo no existe";
					}
				}else {
					echo "Debes establecer una ruta para leer el directorio";
				}
			}

		}
		public static function descargas() {
			// Instrucciones para construir la página descargas
			if (self::$condicion["descargas"] === "verdadero") {
				$fichero = "php/html/descargas.web";
				if (file_exists($fichero)) {
					echo file_get_contents($fichero);
				}
			}
		}
		public static function contactos() {
			// Instrucciones para construir la página contactos
			if (self::$condicion["contactos"] === "verdadero") {
				$fichero = "php/html/contactos.web";
				if (file_exists($fichero)) {
					echo file_get_contents($fichero);
				}
			}
		}
	}

	// Establecer las peticiones de usuario
		paginas::$condicion['nosotros'] = cadena::peticion($peticiones['nosotros'], 'get', 0);
		paginas::$condicion['metodologia'] = cadena::peticion($peticiones['metodologia'], 'get', 0);
		paginas::$condicion['talleres'] = cadena::peticion($peticiones['talleres'], 'get', 0);
		paginas::$condicion['galeria'] = cadena::peticion($peticiones['galeria'], 'get', 0);
		paginas::$condicion['descargas'] = cadena::peticion($peticiones['descargas'], 'get', 0);
		paginas::$condicion['contactos'] = cadena::peticion($peticiones['contactos'], 'get', 0);
	?>

	<?php
	// Protocolo
	$protocolo = @$_SERVER['REQUEST_SCHEME'];
	if ( strtolower($protocolo) === "http" ) {
		$server = @$_SERVER['SERVER_NAME'];
		$uri = @$_SERVER['REQUEST_URI'];

		if ( strtolower($server) === "psicogerencia.com.ve" ) {
			header("Location: https://www.$server$uri");
		}

		// Redirige automáticamente a la ruta
		header("Location: https://$server$uri");
	}
	?>