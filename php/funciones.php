<?php
/*
	Recurso: funciones.php
	Fecha: 12 de enero de 2017
	Desarrollador: David E. Luna M.
	Correo electrónico: davidlunamontilla@gmail.com
	
	Esta es una biblioteca que dispone de un conjunto de objetos y funciones listas para usar
	en las aplicaciones Web que se quiera utilizar.
 */
require_once("config.php");


//require_once("phpqrcode/qrlib.php"); //require_once("dompdf/dompdf_config.inc.php");
// $fpdf = dirname(__FILE__) . "/fpdf/fpdf.php";

/*
if (file_exists($fpdf)) {
	// require($fpdf);
} // */

	class cadena {
		public static function filtrar($string = "") {
			$contenido = $string;

			// Filtrar caracteres HTML
			$contenido = htmlentities($contenido);
			$contenido = htmlspecialchars($contenido);

			// Vectores de reemplazo de caracteres
			$o = array(); $d = array();

			$o[] = "'";
			$o[] = '"';
			$o[] = "<";
			$o[] = ">";
			$o[] = "«";
			$o[] = "»";

			$d[] = '&#39;'; 	// Apóstrofe
			$d[] = '&quot;'; 	// Comilla doble
			$d[] = "&lt;";		// Menor que
			$d[] = "&gt;";		// Mayor que
			$d[] = "&laquo;";	// Comillas anguladas de apertura
			$d[] = "&raquo;";	// Comillas angulas de cierre

			$contenido = str_replace($o, $d, $string);
			return $contenido;
		} // cadena()

		// Para ser utilizado en peticion($array = array(), $string = "")
		protected static function pet($array = array(), $string = "") {
			$get = array(); $post = array();

			// Obteniendo los valores de las variables
			$post = $_POST;
			$get = $_GET;

			switch ($string) {
				case 'post':
					// Petición método POST
						if (is_array($array)) {
							if (count($array) > 0) {
								if (array_keys($post) === $array) {
										return "verdadero";
								}else {
										return "falso";
								}
							}
						}
					break;
				
				default:
					// Petición método GET
						if (is_array($array)) {
							if (count($array) > 0) {
								if (array_keys($get) === $array) {
										return "verdadero";
								}else {
										return "falso";
								}
							}
					}
					break;
			}
		} // Filtro de peticiones self::pet($array = array())

		public static function peticion($array = array(), $string = "", $sesion = 1) {
			if ($sesion === 1) {
				if (sesion::token()[0] === sesion::$tsesion) {
					return self::pet($array, $string);
				} // Comprobando la sesión de usuario
			}else {
				return self::pet($array, $string);
			} // Comprueba si el usuario quiere enviar una petición con la sesión activa o no
		} // cadena::peticion

	} // Clase que permite filtrar las cadenas de textos.

	class estados { // » Referencias especialmente a países 
		// Propiedades
			protected static $estados = array();
			protected static $cadena = "";
			public static $estFuente = "bibliotecas/estados.txt";

		// __Constructores
			public function __construct() {}
		
		// Métodos Públicos
			public static function estados ($array = array()) {
				// Variables
				$campos = array();
				$columnas = array();

				$pais = array();
				$entidad = array();
				$municipio = array();
				$parroquia = array(); // » En desarrollo

				if (file_exists(self::$estFuente)) {
					self::$cadena = cadena::filtrar(file_get_contents(self::$estFuente));
				}else {
					self::$cadena = "El archivo &quot;estados.txt&quot; no existe";
				}

				if (!empty(self::$cadena)) {
					$campos = preg_split("/([\n])/", self::$cadena);
					$campos = array_filter($campos, "strlen");

					foreach($campos as $campo) {
						$columnas = preg_split("/([\,])/", $campo);

						if (is_array($columnas)) {
							
							// Almacenar todos los países del mundo
								if (array_key_exists(0, $columnas)) {
									$pais[] = cadena::filtrar($columnas[0]);
								}

							// Almacenar todos las entidades asociadas a un país
								if (array_key_exists(1, $columnas)) {
									$entidad[strtolower(cadena::filtrar($columnas[0]))][] = cadena::filtrar($columnas[1]);
								}

							// Almacenar todos los municipios de las antidades asociadas
								if (array_key_exists(2, $columnas)) {
									$municipio[strtolower(cadena::filtrar($columnas[0]))][strtolower(cadena::filtrar($columnas[1]))][] = cadena::filtrar($columnas[2]);
								}

							// Almacenar todas las parroquias asociadas a un municipio
								if (array_key_exists(3, $columnas)) {
									$parroquia[strtolower(cadena::filtrar($columnas[0]))][strtolower(cadena::filtrar($columnas[1]))][cadena::filtrar(strtolower($columnas[2]))][] = cadena::filtrar($columnas[3]);
								}
						}
					} // Bucle que se encarga de procesar los valores
					
					$pais = array_unique($pais);
					sort($pais);

					$p = ""; $e = ""; $m = ""; $pa = "";
					if (is_array($array)) {
						$col = count($array);

						if (array_key_exists(0, $array)) {
							$p = $array[0];
							$p = cadena::filtrar($p);
							$p = strtolower($p);

							if (array_key_exists(1, $array)) {
								$e = $array[1];
								$e = cadena::filtrar($e);
								$e = strtolower($e);

								if (array_key_exists(2, $array)) {
									$m = $array[2];
									$m = cadena::filtrar($m);
									$m = strtolower($m);

									if (array_key_exists(3, $array)) {
										$pa = $array[3];
										$pa = cadena::filtrar($pa);
										$pa = strtolower($pa);
									}
								}
							}
						}

						switch ($col) {
							case 1:
								if (array_key_exists($p, $entidad)) {
									if (is_array($entidad[$p])) {
										$entidad[$p] = array_unique($entidad[$p]);
										sort($entidad[$p]);
										return $entidad[$p];
									}
								}
								break;
							
							case 2: // Municipios de los estados provincias asociados a determinados países
								if (array_key_exists($p, $municipio)) {
									if (is_array($municipio[$p])) {
										if (array_key_exists($e, $municipio[$p])) {
											if (is_array($municipio[$p][$e])) {
												$municipio[$p][$e] = array_unique($municipio[$p][$e]);
												sort($municipio[$p][$e]);
												return $municipio[$p][$e];
											}
										}
									}
								}
								break;

							case 3: // Parroquias asociadas a un municipio en función de una región determinada
								if (array_key_exists($p, $parroquia)) {
									if (is_array($parroquia[$p])) {
										if (array_key_exists($e, $parroquia[$p])) {
											if (is_array($parroquia[$p][$e])) {
												if (array_key_exists($m, $parroquia[$p][$e])) {
													if (is_array($parroquia[$p][$e][$m])) {
														$parroquia[$p][$e][$m] = array_unique($parroquia[$p][$e][$m]);
														sort($parroquia[$p][$e][$m]);
														return $parroquia[$p][$e][$m];
													}
												}
											}
										}
									}
								}
								break;
							default:
								if (is_array($pais)) {
									return $pais;
								}
								break;
						}

					} // Verificación de si es un array
				} // if () {}
			}

	} // estados()

	class contenido {
		// Propiedades protegidas o privadas
		protected $title = array();
		protected $conectar = "";
		protected $desconectar = "";

		// Propiedades públicas
		public $tpredeterminado = "";

		// Métodos
		public function establecerTitulo($modulo = "", $titulo = "") {
			$this -> title[cadena::filtrar($modulo)] = cadena::filtrar($titulo);
		}

		// Obtener el título obtenidos por módulos
		public function obtenerTitulo($organizacion = "") {
			if (!empty($organizacion)) $organizacion = " - " . cadena::filtrar($organizacion);
			// if (!empty($this -> tpredeterminado)) $this -> tpredeterminado .= " - ";
			if (empty($this -> tpredeterminado)) $organizacion = cadena::filtrar(str_replace("-", "", $organizacion));

			if (isset($_GET['modulo'])) {
				if (array_key_exists(cadena::filtrar($_GET['modulo']), $this -> title)) { 
					echo $this -> title[cadena::filtrar($_GET['modulo'])] . $organizacion;
				}else {header("Location: ./");}
			}else {
				echo cadena::filtrar($this -> tpredeterminado); // Título predeterminado
			}
		} // Fin | contenido ();

		// Aplicar los métodos de conexión con el servidor de base de datos
		public function conectar($host = "", $usuario = "", $clave = "", $bd = "") {
			$conectar = new mysqli(cadena::filtrar($host), cadena::filtrar($usuario), cadena::filtrar($clave), cadena::filtrar($bd)); // Objeto MySQL
		}
		public function desconectar() { /* Cerrar conexión con el servidor de base de datos*/}

	} // class » contenido
	$contenido = new contenido();

// Creación de imágenes
	class imagen {
		public static $imagen = "captcha.png";

		protected function __construct() {}

		public static function imagenCaptcha() {
			$valor1 = 0; $valor2 = 0;

			// Dimensiones de la imagen
				$x = 100;
				$y = 55;

			// Valores para posterior operación matemática
				$valor1 = rand(0,10);
				$valor2 = rand(0,10);

			// Ejecutar pruebas en la generación de caracteres	
				$operador = "";
				$cadena = "*-+/";
				$numCadena = strlen($cadena);

			// Color de fuente
				$cf = array();
				$cf[] = 250;	// Rojo
				$cf[] = 105;	// Verde
				$cf[] = 020;	// Azul

			// Color de fondo
				$ff = array();
				$ff[] = 255;	// Rojo
				$ff[] = 255;	// Verde
				$ff[] = 255;	// Azul

			// Inicializar Variable
				$i = 0;

			// Establecer cantidad máxima de caracteres
				$max = 1;

			// Calibrar la cantidad de caracteres que se deben mostrar
				$max = $max - 1;

				for ($i = 0; $i < $numCadena; $i++) {
					if ($i <= $max) {
						$operador .= $cadena[rand(0, $numCadena - 1)];
					}
				}

			// Ejecutar operaciones matemáticas
				$residuo = 0;

				switch ($operador) {
					case "-": // Operación de sustracción
						$valor1 = rand(5, 10);
						$valor2 = rand(0, 5);
						break;
					case "/": // Facilitar la operación de división
						do {
							$valor1 = rand(0, 10);
							$valor2 = rand(1, 10);

							$residuo = $valor1 % $valor2;
						} while ($residuo > 0);

						break;
				}

				$num1 = $valor1;
				$num2 = $valor2;

				$resultado = 0;
				$resultado = eval("return $num1 $operador $num2;");

				$texto = "$num1 $operador $num2 = ";

				$texto = preg_replace("/([*])/", "x", $texto);
				$texto = preg_replace("/([\/])/", utf8_decode("÷"), $texto);

				//$texto = "Residuo: $residuo";

				$im = "";

				if (file_exists(self::$imagen)) {
					$im = @imagecreatefrompng(self::$imagen); // Crea una imagen a partir de una existente
				}

				// Establecer las cookies para la comprobación de valores
					$cookie = md5($resultado) . sha1("resultados");
					$cookie = strtoupper($cookie);

				// Enviar una cookie
					setcookie('CAP', $cookie, time() + 1800, "/");
				
			if (!$im) {
				header("content-type: image/png");
				$im = imagecreatetruecolor($x, $y);
				$colorFondo = ImageColorAllocate($im, $ff[0], $ff[1], $ff[2]);
				$colorFuente = ImageColorAllocate($im, $cf[0], $cf[1], $cf[2]);

				imagefilledrectangle($im, 0, 0, $x, $y, $colorFondo);
				imagestring($im, 5, 20, 20, $texto, $colorFuente);

				imagestring($im, 5, 20, 20, $texto, $colorFuente);
				imagepng($im);
				imagedestroy($im);
			}else {
				header("content-type: image/png");
				$colorFondo = ImageColorAllocate($im, $ff[0], $ff[1], $ff[2]);
				$colorFuente = ImageColorAllocate($im, $cf[0], $cf[1], $cf[2]);
				
				imagestring($im, 5, 20, 20, $texto, $colorFuente);
				imagepng($im);
				imagedestroy($im);
			} // imagestring($imagen, $fuente, $x, $y, $cadena, $color); */

		} // imagenCaptha

		// Comprobar captchas
			public static function comprobar() {
				if (isset($_POST['captcha'])) {
					$captcha = cadena::filtrar($_POST['captcha']);
					$cookie = "";

					// Algoritmos sobre lo que se tiene que trabajar
						/*
						$cookie = md5($resultado);
						$cookieValor = md5($resultado) . sha1("resultados");
						// */


						if (is_numeric($captcha)) {
							$captcha = md5($captcha) . sha1("resultados");
							$captcha = strtoupper($captcha);
						}

						if (isset($_COOKIE['CAP'])) {
							$cookie = cadena::filtrar($_COOKIE['CAP']);
						}


						if ($cookie != $captcha) {
							return "<strong>No ha superado la prueba para continuar enviando el formulario</strong>";
						}else {
							return true;
						}
				}
			}


		// Subir imágenes
			public static function subir($key = "", $ext = array(), $fecha = "") {
				if (empty($fecha)) {
					$fecha = date("Y-m");
				}
				$f = preg_split("/[-]/", $fecha);

				if (!empty($key)) {
					if (array_key_exists($key, $_FILES)) {
						// Ruta de subida
							$upload = "./subidas/";
							$path = "";
							$ficheros = array();

						// Crear el directorio en función de la fecha
							if (file_exists($upload)) {
								
								if (!file_exists("$upload$f[0]")) {
									mkdir("$upload$f[0]");

									if (!file_exists("$upload$f[0]/$f[1]")) {
										mkdir("$upload$f[0]/$f[1]");
										$path = "$upload$f[0]/$f[1]/";
									}
								}else {
									$path = "$upload$f[0]/$f[1]/";
								}
							}

							if (isset($path)) {
								if (file_exists($path)) {
									$upload = $path;
								}
							}

						if (is_array($_FILES[$key]['name'])) {
							foreach ($_FILES[$key]['error'] as $k => $error) {
								if ($error == UPLOAD_ERR_OK) {
									// Obtener Extensión del archivo
										$extension = array();
										$extension = preg_split("/[\.]/", $_FILES[$key]['name'][$k]);
										$extension = $extension[count($extension) - 1];

									$nombre = strtoupper(sha1($_FILES[$key]['name'][$k]));
									$nombre_temporal = $_FILES[$key]['tmp_name'][$k];
									$tamanno = $_FILES[$key]['size'][$k];
									$tipo = $_FILES[$key]['type'][$k];

									$ficheros['tipo'][$k] = $tipo;
									$ficheros['nombre'][$k] = $nombre . "." . $extension;
									$ficheros['tamanno'][$k] = $tamanno;
									$ficheros['ruta'][$k] = $upload;
									$ficheros['error'][$k] = $error;

									if (is_array($ext)) {
										foreach ($ext as $ky => $t) {
											if ($tipo === $t) {
												move_uploaded_file($nombre_temporal, $upload . $nombre . "." . $extension);
											}
										}
									}else {
										if ($tipo === $ext) {
											move_uploaded_file($nombre_temporal, $upload . $nombre . "." . $extension);
										}
									}
								}
							}

							return $ficheros;
						}else {
							// Obtener Extensión del archivo
								$extension = array();
								$extension = preg_split("/[\.]/", $_FILES[$key]['name']);
								$extension = $extension[count($extension) - 1];

							$ficheros = array();
							$nombre = strtoupper(sha1($_FILES[$key]['name']));
							$nombre_temporal = $_FILES[$key]['tmp_name'];
							$tamanno = $_FILES[$key]['size'];
							$tipo = $_FILES[$key]['type'];

							$ficheros['tipo'] = $tipo;
							$ficheros['nombre'] = $nombre . "." . $extension;
							$ficheros['tamanno'] = $tamanno;
							$ficheros['ruta'] = $upload;
							$ficheros['error'] = $_FILES[$key]['error'];

							if ($_FILES[$key]['error'] == UPLOAD_ERR_OK) {
								move_uploaded_file($nombre_temporal, $upload . $nombre . "." . $extension);
							}

							return $ficheros;
						}
					}
				}
			}

	} // Class imagen

	class mensajes {
		public static $msg = "";
		public function __construct() {}

		public static function msg($msg = "", $opt = "") {
			$contenido = "";

			if (!empty($msg)) {
				$_SESSION['msg'] = cadena::filtrar($msg);
				$opt = strtolower($opt);

				switch ($opt) {
					case 'p':
						$_SESSION['opt'] = "p";
						break;
					
					case 'n':
						$_SESSION['opt'] = "n";
						break;
				}

			}else {
				if (isset($_SESSION['opt'])) {
					if (isset($_SESSION['msg'])) {
						switch ($_SESSION['opt']) {
							case 'p':
								$contenido = "<strong class=\"verde\">" . cadena::filtrar($_SESSION['msg']) . "</strong>";
								break;
							
							case 'n':
								$contenido = "<strong class=\"rojo\">" . cadena::filtrar($_SESSION['msg']) . "</strong>";
								break;
						}
					}
				}
			}

			return $contenido;
		} // msg()
	} // class mensajes


	class pdf {
		public static $dpdf = "";

		public function __construct() {}
		public static function dpdf() {

		}

	} // class pdf


	// Ejecutar una prueba
		//mensajes::msg(); // Debería mostrar el mensaje

// ======================= | Iniciar Sesión | ========================= //
	class sesion { // Inicio de sesión de usuario
		// Propiedades
			protected static $sActiva = ""; // Se generará un token
			public static $tsesion = "";


		// Token
			protected static $token = "";
			protected static $token1 = "";
			protected static $token2 = "";
			protected static $token3 = "";
			protected static $token4 = "";

			protected static $ncookie1 = "";
			protected static $ncookie2 = "";
			protected static $ncookie3 = "";
			protected static $ncookie4 = "";


		// Métodos
			public function __construct(){}

			protected static function cookies() {
				// Establecer nombre variables	
					$ncookie1 = "1";
					$ncookie2 = "2.2";
					$ncookie3 = "3.1.2";
					$ncookie4 = "1.5.2";

				// Crear una suma de verificación en cada una
					$ncookie1 = strtoupper(md5($ncookie1));
					$ncookie2 = strtoupper(md5($ncookie2));
					$ncookie3 = strtoupper(md5($ncookie3));
					$ncookie4 = strtoupper(md5($ncookie4));

				// Establecer los valores a las propiedades
					self::$ncookie1 = $ncookie1;
					self::$ncookie2 = $ncookie2;
					self::$ncookie3 = $ncookie3;
					self::$ncookie4 = $ncookie4;
			}

			protected static function generadorToken($array = array()) {
				$hash = "";
				$i = 0;

				if (is_array($array)) {
					foreach ($array as $h) {
						if (!empty($h)) {
							$hash .= strtoupper(sha1($h));
						}
					}
					sort($array);

					foreach ($array as $h) {
						if (!empty($h)) {
							for ($i = 0; $i < 10; $i++) {
								$hash .= strtoupper(sha1(sha1($h . "$i")));
							}
						}
					}
				}

				return $hash;
			}

			// Crear variables de sesión o cookies como variables de sesión según sea el caso.
			public static function sesion () {
				self::cookies();
				$usuario = ""; $clave = "";
				if (isset($_POST['usuario']) && isset($_POST['clave'])) {
					$usuario = cadena::filtrar($_POST['usuario']);
					$clave = strtoupper(md5(md5(cadena::filtrar($_POST['clave']))));
					$sActiva = "";

					$resultado = sql::consulta("select * from usuarios where usuario = '$usuario' and clave = '$clave';");
					while ($registro = mysqli_fetch_array($resultado)) {
						$sActiva = date("Y,md,wh:i:s") . sha1(sha1($registro['usuario'])) . sha1(sha1($registro['clave']));
					}
				}

				if (isset($sActiva)) {
					if (!empty($sActiva)) {
						self::$sActiva = strtoupper(md5(md5($sActiva)));
						$token = self::$sActiva;

						// Establecer las propiedades en función de la sesión
							self::$token1 = strtoupper(md5(self::$ncookie1 . $token . sha1($token)));
							self::$token2 = strtoupper(md5(self::$ncookie2 . $token . sha1($token)));
							self::$token3 = strtoupper(md5(self::$ncookie3 . $token . sha1($token)));
							self::$token4 = strtoupper(md5(self::$ncookie4 . $token . sha1($token)));

						// El usuario quiere mantener la sesión abierta
						if (isset($_POST['chkSesion'])) {
							$servidor = $_SERVER['SERVER_NAME'];

							if (!empty($_POST['chkSesion'])) {									

								// Eliminar todas las cookies existentes
									foreach (array_keys($_COOKIE) as $cookie) {
										if ($cookie != "PHPSESSID") {
											setcookie($cookie, $_COOKIE[$cookie], time() - 3600 * 24 * 30, "/", $servidor, false, true);
										}
									}

								// Crear las cookies para sesiones
									setcookie(self::$ncookie1, self::$token1, time() + 3600 * 24 * 30, "/", $servidor, false, true);
									setcookie(self::$ncookie2, self::$token2, time() + 3600 * 24 * 30, "/", $servidor, false, true);
									setcookie(self::$ncookie3, self::$token3, time() + 3600 * 24 * 30, "/", $servidor, false, true);
									setcookie(self::$ncookie4, self::$token4, time() + 3600 * 24 * 30, "/", $servidor, false, true);
							}
						}else {
							// Eliminar todas las cookies existentes
								foreach (array_keys($_COOKIE) as $cookie) {
									if ($cookie != "PHPSESSID") {
										setcookie($cookie, $_COOKIE[$cookie], time() - 3600 * 24 * 30);
									}
								}
						} // Mantener la sesión abierta

						// Establecer valores en la variables de sesiones
							// Token 1
								if (isset($_COOKIE[self::$ncookie1])) {
									if (!empty($_COOKIE[self::$ncookie1])) {
										$_SESSION[self::$ncookie1] = $_COOKIE[self::$ncookie1];
									}
								}else {
									$_SESSION[self::$ncookie1] = self::$token1;
								}

							// Token 2
								if (isset($_COOKIE[self::$ncookie2])) {
									if (!empty($_COOKIE[self::$ncookie2])) {
										$_SESSION[self::$ncookie2] = $_COOKIE[self::$ncookie2];
									}
								}else {
									$_SESSION[self::$ncookie2] = self::$token2;
								}


							// Token 3
								if (isset($_COOKIE[self::$ncookie3])) {
									if (!empty($_COOKIE[self::$ncookie3])) {
										$_SESSION[self::$ncookie3] = $_COOKIE[self::$ncookie3];
									}
								}else {
									$_SESSION[self::$ncookie3] = self::$token3;
								}

							// Token 4
								if (isset($_COOKIE[self::$ncookie4])) {
									if (!empty($_COOKIE[self::$ncookie4])) {
										$_SESSION[self::$ncookie4] = $_COOKIE[self::$ncookie4];
									}
								}else {
									$_SESSION[self::$ncookie4] = self::$token4;
								}


							self::$token = self::generadorToken(array(self::$token1,self::$token2,self::$token3,self::$token4));
							sql::consulta("update usuarios set token = '" . self::$token . "' where usuario = '$usuario' and clave = '$clave';");

							if (isset($_POST['usuario'])) {
								header("Location: ./");
							}
					} // Se creó un hash de sesión
				}
			}

			public static function token () {
				self::cookies();
				$tsesion = md5(true . date("YMh:i:s"));
				self::$tsesion = strtoupper($tsesion);

				self::$token = "";
				if (isset($_SESSION[self::$ncookie1]) and isset($_SESSION[self::$ncookie2]) and isset($_SESSION[self::$ncookie3]) and isset($_SESSION[self::$ncookie4])) {
					if (!empty($_SESSION[self::$ncookie1]) && !empty($_SESSION[self::$ncookie2]) && !empty($_SESSION[self::$ncookie3]) && !empty($_SESSION[self::$ncookie4])) {
						self::$token = self::generadorToken(array($_SESSION[self::$ncookie1], $_SESSION[self::$ncookie2], $_SESSION[self::$ncookie3], $_SESSION[self::$ncookie4]));
					}
				}else {
					if (isset($_COOKIE[self::$ncookie1]) and isset($_COOKIE[self::$ncookie2]) and isset($_COOKIE[self::$ncookie3]) and isset($_COOKIE[self::$ncookie4])) {
						if (!empty($_COOKIE[self::$ncookie1]) && !empty($_COOKIE[self::$ncookie2]) && !empty($_COOKIE[self::$ncookie3]) && !empty($_COOKIE[self::$ncookie4])) {
							self::$token = self::generadorToken(array($_COOKIE[self::$ncookie1], $_COOKIE[self::$ncookie2], $_COOKIE[self::$ncookie3], $_COOKIE[self::$ncookie4]));
						}
					}
				}
					$array = array();
					$array[0] = md5(false);
					$array[0] = strtoupper($array[0]);

				// Consultar si el token es válido
					if (!empty(self::$token)) {
						$resultado = sql::consulta("select tipo from usuarios where token = '" . self::$token . "';");

						// Consultar de una sesión válida
							while ($registro = mysqli_fetch_array($resultado)) {
								$array[0] = self::$tsesion;
								$array[1] = $registro['tipo'];
							}
					}else {self::$token = "0000000000";}

				if ($array[0] != self::$tsesion) {
					// Destruir todas las sesiones y cookies existentes
					foreach (array_keys($_COOKIE) as $cookie) {
						if ($cookie != "PHPSESSID") {
							setcookie($cookie, $_COOKIE[$cookie], time() - 3600 * 24 * 30, "/", $_SERVER['SERVER_NAME'], false, false);
						}
					}
					$_SESSION = array();
					if (count($_SESSION) > 0) {session_destroy();}
				}

				return $array;
			} // token

		// Destruir la sesión de usuario
			public static function destruirSesion() {
				// Proceder con la destrucción de las sesiones cuando el usuario la solicite
					if (isset($_POST['destruirSesion'])) {
						self::cookies();
						// Destruir las cookies creadas por el sistemas
							if (count($_COOKIE) > 0) {
								foreach (array_keys($_COOKIE) as $cookie) {
									if ($cookie != "PHPSESSID") {
										setcookie($cookie, $_COOKIE[$cookie], time() - 3600 * 24 * 30, "/", $_SERVER['SERVER_NAME'], false, false);
									}
								}
							}

						// Destruir las sesiones creadas por el sistema
							if (count($_SESSION) > 0) {
								$_SESSION = array();
								session_destroy();
							}

						// Una vez se haya destruido la sesión, se procede volver al principio
							header("Location: ./");	
					}
			} // destruirSesion()

		// Obtener el nombre y apellido de la persona una vez inicia 
			public static function personas($string = "") {
				$token = "";
				if (isset($_SESSION[self::$ncookie1]) and isset($_SESSION[self::$ncookie2]) and isset($_SESSION[self::$ncookie3]) and isset($_SESSION[self::$ncookie4])) {
					if (!empty($_SESSION[self::$ncookie1]) && !empty($_SESSION[self::$ncookie2]) && !empty($_SESSION[self::$ncookie3]) && !empty($_SESSION[self::$ncookie4])) {
						$token = self::generadorToken(array($_SESSION[self::$ncookie1], $_SESSION[self::$ncookie2], $_SESSION[self::$ncookie3], $_SESSION[self::$ncookie4]));
					}
				}else {
					if (isset($_COOKIE[self::$ncookie1]) and isset($_COOKIE[self::$ncookie2]) and isset($_COOKIE[self::$ncookie3]) and isset($_COOKIE[self::$ncookie4])) {
						if (!empty($_COOKIE[self::$ncookie1]) && !empty($_COOKIE[self::$ncookie2]) && !empty($_COOKIE[self::$ncookie3]) && !empty($_COOKIE[self::$ncookie4])) {
							$token = self::generadorToken(array($_COOKIE[self::$ncookie1], $_COOKIE[self::$ncookie2], $_COOKIE[self::$ncookie3], $_COOKIE[self::$ncookie4]));
						}
					}
				}

				if (!empty($token)) {
					$resultado = sql::consulta("select nombres, apellidos from usuarios where token = '" . $token . "' limit 1;");
					$array = array();

					while ($registro = mysqli_fetch_array($resultado)) {
						if (array_key_exists($string, $registro)) {
							$array['nombres'] = $registro['nombres'];
							$array['apellidos'] = $registro['apellidos'];
						}
					}
				}
				
				if (!empty($string)) {
					if (array_key_exists($string, $array)) {
						return $array[$string];
					}else {
						return "Opciones disponibles: nombres | apellidos";
					}
				}
			} // personas()

		public static function formulario ($imagen = "") {
			$contenido = "";
			
			$contenido = '
				<div class="cuentas-usuario">
					<form action="./" method="post" class="sesion">
						<img src="images/apps/' . $imagen . '">
						<div class="texto"><input name="usuario" autofocus="" autocomplete="off" required="" autofocus="" type="text" placeholder="Usuario"></div>
						<div class="texto button-sesion">
							<input name="clave" required="" type="password" placeholder="Contraseña">
							<button type="submit">Acceder</button>
						</div>

						<div class="opciones">
							<div class="checkbox" id="checkbox">
								<input name="chkSesion" type="checkbox"><span class="" tabindex="0">No cerrar sesión</span>
							</div>
							<a href="javascript:;">Recuperar cuenta</a>
						</div>
					</form>
				</div>
				';

			if (self::token()[0] === self::$tsesion) {
				$contenido = "";

				$contenido = '
					<form class="none" action="./" method="post" name="sesion" id="sesion">
						<input type="hidden" name="destruirSesion" value="1" />
					</form>
				';
			}

			return $contenido;
		}
	} // Crear variables de sesión y comprobarlas

// ======================= | FIN | INICIAR SESIÓN | =================== //

?>
