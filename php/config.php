<?php session_start(); header("content-type: text/html; charset=utf-8");
	error_reporting(E_ALL | E_NOTICE | E_STRICT); // Reportar cualquier error encontrado en PHP

	// Establecer Zona Horaria predeterminada
	date_default_timezone_set('America/Caracas');
	
	// Conexión con el servidor de base de datos
	class sql {
		// Propiedades públicas
			public static $host = "";
			public static $usuario = "";
			public static $clave = "";
			public static $bd = "";

		public function __construct() {} // Constructor

		public static function sql () {}

		public static function consulta($string = "") {
			$conexion = new mysqli(self::$host, self::$usuario, self::$clave, self::$bd);

			if (!empty($string)) {
				// Verificar la conexión con el servidor de base de datos
				if ($conexion -> connect_errno) {
					echo "La conexión ha fallado";
					exit(); // Terminar la ejecución de código tras un error de conexión 
				}

				if ($resultado = $conexion -> query($string)) {
					// Cerrar automáticamente tras ejecutar la consulta
					$conexion -> close();

					return $resultado;
				}
			}
		}

		// Consulta rápida en función del nombre de campo
		public static function consulta_rapida($sql ="", $campo = "") {
			if (!empty($sql)) {
				if (!empty($campo)) {
					$resultado = self::consulta($sql);
					while ($registro = mysqli_fetch_array($resultado)) {
						if (array_key_exists($campo, $registro)) {
							return $registro[$campo];
						}
					}
				}
			}
		}
	} // class sql()
	/*
	sql::$host = "";
	sql::$usuario = "";
	sql::$clave = "";
	sql::$bd = "";
	// */
	// Ejecutando consulta de pruebas en una base de datos
?>