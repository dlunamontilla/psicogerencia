<?php
	require __DIR__ . "/php/index.php";
?>

<?php
	// Protocolo
	$protocolo = @$_SERVER['REQUEST_SCHEME'];
	if ( strtolower($protocolo) === "http" ) {
		$server = @$_SERVER['SERVER_NAME'];
		$uri = @$_SERVER['REQUEST_URI'];

		if ( strtolower($server) === "psicogerencia.com.ve" ) {
			header("Location: https://www.$server$uri");
		}

		// Redirige automáticamente a la ruta
		header("Location: https://$server$uri");
	}
	?>
	
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta http-equiv="X-UA-Compatible" content="ie=edge" />
	
	<!-- Colores en la barra de direcciones del navegador -->
	<meta name="theme-color" content="#8f0058" />
	<link rel=icon href="imagen/favicon-fondo.png" sizes="128x128" type="image/png" />
	<link rel=icon href="imagen/favicon-fondo.svg" sizes="any" type="image/svg+xml" />

	<title>Psicogerencia</title>

	<link rel="stylesheet" type="text/css" href="css/index.css">

	<!-- Metaetiquetas -->
	<meta http-equiv="PRAGMA" content="NO-CACHE" />
	<meta http-equiv="CACHE-CONTROL" content="NO-CACHE,NO-STORE,MUST-REVALIDATE" />
	<meta name="GOOGLEBOT" content="INDEX,FOLLOW" />
	<meta name="ROBOTS" content="INDEX,FOLLOW,ALL,NO-ARCHIVE" />
	<meta name="REVISIT" content="7 days" />
	<meta name="NOW" content="2014-01-13 18:00" />
	<meta name="REVIEW" content="2014-01-13" />
	<meta name="REVISIT-AFTER" content="7 DAYS" />
	<meta name="Language" content="Spanish">
	<link rel="shortcut icon" href="imagen/favicon.png" type="image/x-icon"/>

	<!-- Plugins -->
	<link rel="stylesheet" href="plugins/fontawesome-free-5.5.0-web/css/all.css">

	<!-- Script | JavaScript -->
	<script src="script/biblioteca.js" type="text/javascript" charset="utf-8"></script>
	<script src="script/script.js" type="text/javascript" charset="utf-8"></script>

</head>
<body>
<main role="main" class="wrapper" id="wrapper">
	
	<header id="header" class="header">
		<img src="imagen/ivpsi.png" class="logotipo" alt="Instituto Venezolano de Psicogerencia" />
		<img src="imagen/ojo-tramado.png" class="tramado" alt="Tramado y ojo" />
	</header><!-- /header -->

	<section id="section" class="section">
	<?php
		paginas::inicio();
		paginas::nosotros();
		paginas::metodologia();
		paginas::talleres();
		if (paginas::$condicion["galeria"] === "verdadero") {?>
			<div class="galeria">
				<h3>Galeria</h3>
			</div>
	<?php
		}
		paginas::descargas();
		paginas::contactos();
	?>
	</section>
	
	<footer id="footer" class="footer">
		<?php paginas::menu(); ?>
	</footer>
</main> <!-- wrapper -->

<div class="ventana-modal none" id="ventana-modal">
	<div class="title" id="title">
		<div class="title">&nbsp;</div>
		<div class="cerrar">x</div>
	</div>
	<div class="section" id="section"><img src="imagen/galerias/PARTE 1/foto-4.png" alt="" /></div>
	<div class="footer" id="footer">&nbsp;</div>
	<div class="back" id="back"><img src="imagen/volver.png" alt="Volver" /></div>
	<div class="next" id="next"><img src="imagen/avanzar.png" alt="Avanzar" /></div>
</div>
<?php
	if (paginas::$condicion["galeria"] === "verdadero") {
	?>
		<div class="galeria-no-disponible"></div>
	<?php
	}
	if (count($_GET) === 0) {?>
		<div class="aviso" id="aviso">
			<div class="title"><div class="cerrar" id="cerrar">x</div></div>
			<div class="aviso-contenido"><img src="imagen/aviso.jpeg" alt="Aviso"></div>
			<div class="footer"></div>
		</div>
	<?php }
?>
</body>
</html>