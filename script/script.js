$.leer(document, function () {
	// Cerrar la ventana modal
	$.click(".cerrar", function() {
		var ventanaModal = document.querySelector("#ventana-modal");
		if (ventanaModal !== null) {
			ventanaModal.className = "none";
		}

		// Cerrar la ventana modal de aviso
		var aviso = document.querySelector("#aviso");
		if (aviso !== null) {
			aviso.className = "aviso none";
		}
	});

	// Menú contextual
	$.click("#menu-contextual div.minre", function () {
		var contextual = document.querySelector("#menu-contextual");
		if (contextual !== null) {
			if (contextual.className === "menu") {
				contextual.className = "lista-menu";
			}else {
				contextual.className = "menu";
			}
		}
	});

	// Cerrar el menú al presionar el enlace
	$.click("#menu-contextual a", function(e) {
		e.preventDefault();
		var href = this.href;

		var contextual = document.querySelector("#menu-contextual");
		if (contextual !== null) {
			contextual.className = "menu";
			$.transitionend("#menu-contextual", function () {
				parent.location.href = href;
			});
			
		}
	});
});

(function () {
	function psi(objectDOM) {
		const event = function ( event, fn ) {

			if ( typeof objectDOM !== "undefined" && typeof objectDOM !== "object" ) {
				objectDOM = document.querySelectorAll(objectDOM);
				if ( objectDOM.length > 0 ) {
					objectDOM.forEach(function ( DOM ) {
						DOM.addEventListener(event, fn, false);
					});
				}
			}else {
				if ( typeof objectDOM !== "undefined" && typeof objectDOM !== "null" ) {
					if ( typeof objectDOM.addEventListener === "function" ) {
						objectDOM.addEventListener(event, fn, false);
					}else {
						if (typeof objectDOM.length === "number" && objectDOM.length > 0) {
							objectDOM.forEach(function ( DOM ) {
								DOM.addEventListener(event, fn, false);
							});
						}
					}
				}
			}

		}

		let pro = {
			// Eventos
			leer : function ( fn ) {
				if ( typeof objectDOM.addEventListener === "function" ) {
					objectDOM.addEventListener("readystatechange", function () {
						if ( objectDOM.readyState === "complete" ) {
							fn();
						}
					}, false);
				}
			},

			click : function ( fn ) {
				event( "click", fn );
			},

			onkeypress : function ( fn ) {
				event( "keypress", fn );
			},

			resize : function ( fn ) {
				event( "resize", fn );
			}
		}


		return pro;
	}

	// Lectura de documento
	psi(document).leer(function () {
		let rootStyle = document.documentElement.style;

		const height = function (elemento) {
			if ( typeof elemento !== "undefined" ) {
				let header = document.querySelectorAll(elemento);
				
				if ( typeof header === "object" && typeof header.length === "number" ) {
					if ( header.length > 0 ) {
						header.forEach(function ( DOM ) {
							rootStyle.setProperty("--top", DOM.offsetHeight + "px");
							console.log( DOM.offsetHeight );
						});
					}
				}
			}

		}

		height("#header");

		psi(window).resize(function () {
			height("#header");
		});
	});

}());