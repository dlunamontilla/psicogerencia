/*
	Recurso: biblioteca.js
	Fecha: 4 de enero 2015
	Desarrollador: David E Luna M
*/
// Ejecutar unas pruebas de ejecución con javascript
(function (window, undefined) {
	var document = window.document,
		location = window.location,
		navigator = window.navigator;

		var core = (function () {
			var numeros = {
				leer : function (objeto, funcion) {
					if (objeto === document) {
						document.onreadystatechange = function () {
							var estatus = document.readyState;
							
							if (document.readyState) {
								switch (estatus) {
									case "interactive": // Ya se puede interactuar con los elementos
										funcion();
										break;
									case "complete": // Soporte a Internet Explorer
										if (navigator.appName === "Microsoft Internet Explorer") {
											funcion();	
										}
										break;

								}
							}
						}
					}
				},

				cargar : function (objeto, funcion) {
					if (objeto === document) {
						document.onreadystatechange = function () {
							var estatus = document.readyState;

							if (document.readyState) {
								switch (estatus) {
									case "complete": // Ya se puede interactuar con los elementos
										funcion();
										break;
								}
							}else { 
								funcion();
							}

						}
					}
				}, // cargar()

				// Incorporación de Ajax en esta zona 
				get : function (url, funcion) {
					// Instanciar XMLHttpRequest
					var peticion = new XMLHttpRequest();


					if (url !== undefined) {
						if (funcion !== undefined) {
							// Solicitar datos mediante petición enviada al servidor
							peticion.open('get', url, true);

							// Obtener los datos solicitados del servidor 
							peticion.onreadystatechange = function () {
								if (this.readyState === 4) {
									if (this.status == 200) {
										contenido = this.responseText;
										funcion(contenido, this.status);
									}else {
										if (this.status === 404) {
											contenido = "La url \"" + url + " \" introducida es inaccesible";
											funcion(contenido, this.status);
										}else {
											contenido = "Se produjo un error desconocido: " + this.status;
											funcion(contenido, this.status);
										}
									}
								}
							}

							// Monitorear el progreso de carga
							peticion.onprogress = function (pr) {
								if (pr.lengthComputable) {
									// funcion;
								}
							};


							// La carga del documento se ha terminado
							peticion.onloadend = function (pr) {
								if (progreso !== null) {
									
								}
							};

							// Enviar la petición 
							peticion.send('null');
						}
					}
				},

				// Evento de formulario 
				post : function (form, funcion) {
					// Ejecutar unas pruebas antes de continuar
					var formulario = document.querySelector(form);

					if (formulario !== null) {

							// Evento de formulario 
							if (formulario.addEventListener) {
								// Sobrescribir el mismo evento
								formulario.addEventListener('submit', function (e) {
									
									enviarFormulario(this);
									e.preventDefault();

								}, false); // */

								// Ejecutar una prueba relacionado this

							}else if (formulario.attachEvent) {
								formulario.attachEvent('onsubmit', function (e) {
									e.preventDefault();
									enviarFormulario(this);
								}, false);
							}

							
							var enviarFormulario = function (obj) {
								if (funcion !== undefined) {
									// Crear una instancia de formulario 
									var formData = new FormData(obj);

									// Crear una instancia de XMLHttpRequest()
									var peticion = new XMLHttpRequest();

									// Enviar una petición al servidor
									peticion.open('post', formulario.action, true);

									// Recibir datos del servidor a través de una petición enviada
									peticion.onreadystatechange = function () {
										if (peticion.status === 200) {
											if (peticion.readyState === 4 ) {
												funcion(this.responseText, this.status);
											}
										}
									}

									// Enviar la petición al servidor
									peticion.send(formData); // Enviar el formulario
								}
							} // enviarFormulario */

					} // Si se ha definido una url
				},

				// -------------- Eventos de JavaScript --------------- //
				// 1. El usuario hace clic
				click : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('click', funcion, false);
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('onclick', funcion, false);
								}
							}
						}
					}
				}, // click
				toque : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('touchstart', funcion, false);
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('ontouchstart', funcion, false);
								}
							}
						}
					}
				}, // toque
				
				// Redimensionar la ventana del navegador
				resize : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('resize', funcion, false);
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('onresize', funcion, false);
								}
							}
						}
					}
				}, // Redimensionar Ventanad

				// 2. Se genera cambio en el valor de un elemento
				change : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('change', funcion, false);
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('onchange', funcion, false);
								}
							}
						}
					}
				},
				// 3. Se genera cambio en el valor de un elemento
				blur : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('blur', funcion, false);
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('onblur', funcion, false);
								}
							}
						}
					}
				},

				// Eventos del Teclado
				// 4. Se genera cambio en el valor de un elemento
				keypress : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('keypress', funcion, false);
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('onkeypress', funcion, false);
								}
							}
						}
					}
				},
				keydown : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('keydown', funcion, false);
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('onkeydown', funcion, false);
								}
							}
						}
					}
				},
				keyup : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('keyup', funcion, false);
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('onkeyup', funcion, false);
								}
							}
						}
					}
				},

				// Eventos de animación
				animationend : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('animationend', funcion, false);
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('onanimationend', funcion, false);
								}
							}
						}
					}
				},
				animationstart : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('animationstart', funcion, false);

								// Soporte para otros navegadores
								try {
									el[i].addEventListener('webkitAnimationStart', funcion, false);
								}catch (info) {alert(info);}
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('onanimationend', funcion, false);
								}
							}
						}
					}
				},

				transitionend : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('transitionend', funcion, false);
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('ontransitionend', funcion, false);
								}
							}
						}
					}
				},

				// Eventos de manipulación de vídeo
				ended : function (elemento, funcion) {
					var el = document.querySelectorAll(elemento);

					if (el.length > 0) {
						for (var i = 0; i < el.length; i++) {
							if (el[i].addEventListener) {
								el[i].addEventListener('ended', funcion, false);
							}else {
								if (el[i].attachEvent) {
									el[i].attachEvent('onended', funcion, false);
								}
							}
						}
					}
				}, // Alias » $.finalizado

				// Otros eventos
				abrirVentana : function (elemento, propiedad) {
					var objeto = document.querySelectorAll(elemento);

					if (objeto.length > 0) {
						for (var i = 0; i < objeto.length; i++) {
							if (objeto[i].addEventListener) {
								objeto[i].addEventListener('click', function (e) {
									var url = (propiedad.url !== undefined) ? propiedad.url : "";
										url = (this.getAttribute('href') !== null) ? this.getAttribute('href') : url;

									// Opciones para centrar la ventana
										var horizontal = (propiedad.aHorizontal !== undefined) ? propiedad.aHorizontal : 0;
										var vertical = (propiedad.aVertical !== undefined) ? propiedad.aVertical : 0;
										

									// Elementos del método open del objeto window
										var left = (propiedad.left !== undefined) ? propiedad.left : 0;
										var top = (propiedad.top !== undefined) ? propiedad.top : 0;
										var alto = (propiedad.alto !== undefined) ? propiedad.alto : 500; 
										var ancho = (propiedad.ancho !== undefined) ? propiedad.ancho : 300;

									// Controlar tamaño de la ventana en función de la resolución de pantalla
										var anchoPantalla = screen.width;
										var altoPantalla = screen.height;

									// Controlar el tamaño de la ventana si el elemento es mayor que la misma
									
										if (anchoPantalla <= ancho) {
											ancho = anchoPantalla;
										}
										if (altoPantalla <= alto) {
											alto = altoPantalla;
										} 

									// Centrar la ventana en la pantalla si el usuario lo requiere
										if (horizontal === "centrado") {
											left = 0;
											left = (anchoPantalla / 2) - (ancho / 2);
										}

										if (vertical === "centrado") {
											top = 0;
											top = (altoPantalla / 2) - (alto / 2);
										}
									

									// Elementos del método open del objeto window
										var destino = (propiedad.destino !== undefined) ? propiedad.destino : "System";

									// Verificar si es modal o ventana
										if (destino === "modal") {
											var clase = (propiedad.clase !== undefined) ? propiedad.clase : "";
											var ventana = (propiedad.ventana !== undefined) ? propiedad.ventana : null;
											var imagen = (propiedad.imagen !== undefined) ? propiedad.imagen : null;

											if (ventana !== null) {
												var v = document.querySelector(ventana);
												var i = document.querySelector(imagen);

												if (v !== null) {
													if (i !== null) {
														v.className = clase;

														if (this.href !== undefined) {
															i.src = this.href;
														}else {
															i.remove();
														}
														
													}
												}
											}
										}else {
											// Abrir el objeto Window
											window.open(url, destino, 'top=' + top + ', left=' + left + ', height=' + alto + ', width=' + ancho);
										}
									
									e.preventDefault();
								}, false);
							}else {
								// Soporte para Internet explorer
								if (objeto[i].attachEvent) {
									objeto[i].attachEvent('onclick', function (e) {
										var url = (propiedad.url !== undefined) ? propiedad.url : "";
										url = (this.getAttribute('href') !== null) ? this.getAttribute('href') : url;

									// Opciones para centrar la ventana
										var horizontal = (propiedad.aHorizontal !== undefined) ? propiedad.aHorizontal : 0;
										var vertical = (propiedad.aVertical !== undefined) ? propiedad.aVertical : 0;
										

									// Elementos del método open del objeto window
										var left = (propiedad.left !== undefined) ? propiedad.left : 0;
										var top = (propiedad.top !== undefined) ? propiedad.top : 0;
										var alto = (propiedad.alto !== undefined) ? propiedad.alto : 500; 
										var ancho = (propiedad.ancho !== undefined) ? propiedad.ancho : 300;

										var anchoPantalla = screen.width;
										var altoPantalla = screen.height;


									// Controlar el tamaño de la ventana si el elemento es mayor que la misma
									
										if (anchoPantalla <= ancho) {
											ancho = anchoPantalla;
										}
										if (altoPantalla <= alto) {
											alto = altoPantalla;
										}

									// Centrar la ventana en la pantalla si el usuario lo requiere
										if (horizontal === "centrado") {
											left = 0;
											left = (anchoPantalla / 2) - (ancho / 2);
										}

										if (vertical === "centrado") {
											top = 0;
											top = (altoPantalla / 2) - (alto / 2);
										}

									// Elementos del método open del objeto window
										var destino = (propiedad.destino !== undefined) ? propiedad.destino : "System";

									// Verificar si es modal o ventana
										if (destino === "modal") {
											var clase = (propiedad.clase !== undefined) ? propiedad.clase : "";
											var ventana = (propiedad.ventana !== undefined) ? propiedad.ventana : null;
											var imagen = (propiedad.imagen !== undefined) ? propiedad.imagen : null;

											if (ventana !== null) {
												var v = document.querySelector(ventana);
												var i = document.querySelector(imagen);

												if (v !== null) {
													if (i !== null) {
														v.className = clase;

														if (this.href !== undefined) {
															i.src = this.href;
														}else {
															i.remove();
														}
														
													}
												}
											}
										}else {
											// Abrir el objeto Window
											window.open(url, destino, 'top=' + top + ', left=' + left + ', height=' + alto + ', width=' + ancho);
										}
									
									e.preventDefault();
									}, false);
								}
							}
						}
					}
				}, // abrirVentana() */

				// Cambio de clase
				cambiarClase : function (elementos, propiedad) {
					var num = 0,
						intervalo,
						tf,
						nActual = 0,
						nAnterior = 0,
						claseActual = (propiedad.actual !== undefined) ? propiedad.actual : "",
						claseAnterior = (propiedad.anterior !== undefined) ? propiedad.anterior : "",
						claseIntermedia = (propiedad.intermedio !== undefined) ? propiedad.intermedio : "",
						tmp = (propiedad.tiempo !== undefined) ? parseInt(propiedad.tiempo) : parseInt(0),
						tespera = (propiedad.espera !== undefined) ? parseInt(propiedad.espera) : parseInt(0.3),
						pausa = (propiedad.pausa !== undefined) ? true : (propiedad.pause !== undefined ) ? true : false;

					// Obtener elementos
					var elm = document.querySelectorAll(elementos);
					
					if (elm.length > 1) {

						if (nActual <= 0) {
							num = 0;
							nActual = 0;
							nAnterior = 0;
							// Restablecer todo para el momento de la descarga
							for (var i = 0; i < elm.length; i++) {
								
								if (i != num) {
									elm[i].className = claseAnterior;
								}
								
							}
							
							elm[0].className = claseActual;
						} // No debe ejecutarse si no ha avanzado

						try {
							var cambiar = function () {
								num++;

								if (num === 0) {
									nActual = num;
									nAnterior = elm.length - 1;
								}else if ((num > 0) && (num < elm.length)) {
									nActual = num;
									nAnterior = num - 1;
								}else if (num >= elm.length) {
									num = 0;
									nActual = num;
									nAnterior = elm.length - 1;
								}

								

								elm[nActual].className = claseActual;
								elm[nAnterior].className = claseIntermedia;

								tf = setTimeout(function () {
									elm[nAnterior].className = claseAnterior;
								}, tespera);
							}

							intervalo = setInterval(cambiar, 1000 * tmp);
						}catch (info) {alert(info);}
								
					} // Si obtiene elementos
				}, // cambiarClase

				// Cambiar clases con pausas incluidas
				cCls : function (elementos, propiedad, i) { // Nombre provisional: cCls -> CambiarClase
					var actual = ( propiedad.actual !== undefined ) ? propiedad.actual : "",
					intermedio = ( propiedad.intermedio !== undefined ) ? propiedad.intermedio : "",
					anterior = ( propiedad.anterior !== undefined ) ? propiedad.anterior : "",
					pausa = ( propiedad.pausa !== undefined ) ? propiedad.pausa : false,
					espera = ( propiedad.espera !== undefined) ? parseInt( propiedad.espera ) : 0,
					pausado = ( propiedad.pausado !== undefined ) ? true : false;

					// Actualmente en desarrollo
					var objetos = document.querySelectorAll(elementos);
					if (objetos.length > 0) {
						
					}
				}

			} // objeto números

			return numeros;
		})();

		window.core = window.$ = window.bt = core;

})(window);

// Alias en español
	$.classChange = $.cambiarClase;
	$.soltarTecla = $.keyup;
	$.openWindow = $.abrirVentana; // 
	$.desenfocar = $.blur; // Se dispara el evento al desenfocar un objeto
	$.finanimacion = $.animationend; // Al finalizar una animación se dispara un evento
	$.fintransicion = $.transitionend; //  Al finalizar una transición se disparar un evento

// Alias (evento) - Manipulación de vídeo
	$.finalizado = $.ended; // Al finalizar el vídeo se ejecutará una acción determinada

// Ventana Navegador
	$.redimensionar = $.resize; // El usuario redimensiona la ventana del navegador